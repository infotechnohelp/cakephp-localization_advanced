<?php

namespace Infotechnohelp\Localization\Shell;

use Cake\Console\Shell;
use Cake\Core\Configure;
use Cake\Filesystem\File;
use Cake\Filesystem\Folder;
use Cake\Http\Exception\InternalErrorException;
use Cake\I18n\Parser\PoFileParser;

/**
 * Class LocaleShell
 * @package Infotechnohelp\Localization\Shell
 */
class LocaleShell extends Shell
{
    /**
     * @param string|null $providedLanguageCode
     */
    private function createJsLocale(string $providedLanguageCode = null)
    {
        $languageCode = $providedLanguageCode;

        if (is_null($languageCode)) {
            $languageCode = (new Folder('src' . DS . 'Locale'))->read()[0][0];
        }

        $PoFileParser = new PoFileParser();

        // @todo Should work not only with default.po, but all of them
        $localeFilePath = 'src/Locale/' . $languageCode . '/default.po';

        if (!(new File($localeFilePath))->exists()) {
            throw new InternalErrorException(sprintf(
                'Localization file for %s does not exist or is empty',
                $languageCode
            ));
        }

        $parsedFileContents = $PoFileParser->parse($localeFilePath);


        if (!file_exists('webroot/js/Locale/' . $languageCode)) {
            new Folder(WWW_ROOT . 'js' . DS . 'Locale' . DS . $languageCode, true, 0755);
        }

        $result = [];
        if (is_null($providedLanguageCode)) {
            foreach ($parsedFileContents as $key => $value) {
                $result[$key] = $key;
            }
        } else {
            foreach ($parsedFileContents as $key => $value) {
                $result[$key] = $value['_context'][''];
            }
        }

        if (is_null($providedLanguageCode)) {
            $languageCode = Configure::read('App.defaultLocale');
        }

        if (!file_exists('webroot/js/Locale/' . $languageCode . '/default.js')) {
            new File(WWW_ROOT . 'js' . DS . 'Locale/' . $languageCode . '/default.js', true);
        }

        $file = new File(WWW_ROOT . 'js' . DS . 'Locale/' . $languageCode . '/default.js');
        $file->write("const phrasesJson = '" .
            addcslashes(json_encode($result, JSON_UNESCAPED_UNICODE), "'") .
            "';");


        if (!file_exists('webroot/js/Locale/' . $languageCode . '/default.json')) {
            new File(WWW_ROOT . 'js' . DS . 'Locale/' . $languageCode . '/default.json', true);
        }

        $file = new File(WWW_ROOT . 'js' . DS . 'Locale/' . $languageCode . '/default.json');
        $file->write(json_encode($result, JSON_UNESCAPED_UNICODE));


        $this->out($languageCode . ' js locale created');
    }

    /**
     * @param string|null $providedLanguageCode
     */
    public function js(string $providedLanguageCode = null)
    {
        if ($providedLanguageCode !== null) {
            $this->createJsLocale($providedLanguageCode);
            return;
        }

        $this->createJsLocale();

        foreach ((new Folder('src' . DS . 'Locale'))->read()[0] as $languageCode) {
            $this->createJsLocale($languageCode);
        }
    }
}
