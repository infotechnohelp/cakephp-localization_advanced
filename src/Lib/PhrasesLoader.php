<?php

declare(strict_types=1);

namespace Infotechnohelp\Localization\Lib;

use Cake\I18n\I18n;
use Cake\Routing\Router;

class PhrasesLoader
{
    public static function jsPath(string $locale = null, string $type = 'default')
    {
        if ($locale === null) {
            $locale = I18n::getLocale();
        }

        return sprintf(
            '<script src="%sjs/Locale/%s/%s.js"></script>',
            Router::url('/', true),
            $locale,
            $type
        );
    }
}
