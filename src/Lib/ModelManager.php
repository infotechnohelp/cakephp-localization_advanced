<?php

declare(strict_types=1);

namespace Infotechnohelp\Localization\Lib;

use Cake\I18n\I18n;
use Cake\ORM\Entity;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;

/**
 * Class ModelManager
 * @package Infotechnohelp\Localization\Lib
 */
class ModelManager
{
    public static function createEntity(string $tableAlias, array $entityData = [], array $entityLocalizedData = [])
    {
        /** @var Table $Table */
        $Table = TableRegistry::getTableLocator()->get($tableAlias);

        I18n::setLocale(I18n::getDefaultLocale());

        /** @var Entity $Entity */
        $Entity = $Table->newEntity($entityData);

        foreach ($entityLocalizedData as $lang => $data) {
            $Entity->translation($lang)->set($data, ['guard' => false]);
        }

        return $Table->saveOrFail($Entity);
    }

    public static function updateEntity(
        string $tableAlias,
        int $entityId,
        array $entityData = [],
        array $entityLocalizedData = []
    ) {
        /** @var Table $Table */
        $Table = TableRegistry::getTableLocator()->get($tableAlias);

        I18n::setLocale(I18n::getDefaultLocale());

        /** @var Entity $Entity */
        $Entity = $Table->get($entityId);

        foreach ($entityData as $key => $value) {
            $Entity->set($key, $value);
        }

        foreach ($entityLocalizedData as $lang => $data) {
            $Entity->translation($lang)->set($data, ['guard' => false]);
        }

        // @todo Validate

        return $Table->saveOrFail($Entity);
    }
}
