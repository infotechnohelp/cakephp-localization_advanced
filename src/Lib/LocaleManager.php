<?php


declare(strict_types=1);

namespace Infotechnohelp\Localization\Lib;

use Cake\Filesystem\Folder;
use Cake\I18n\I18n;
use Cake\I18n\MessagesFileLoader;

/**
 * Class LocaleManager
 * @package Core\Lib
 */
class LocaleManager
{
    /**
     * @return array
     */
    public static function getLanguageLabelMap(): array
    {
        return [
            'en_US' => 'ENG',
            'et_EE' => 'EST',
            'ru_RU' => 'RUS',
        ];
    }

    /**
     * @return array
     */
    public static function getLanguageCountryMap(): array
    {
        return [
            'en_US' => 'GreatBritain',
            'et_EE' => 'Estonia',
            'ru_RU' => 'Russia',
        ];
    }

    /**
     * @return array
     */
    public static function getLanguageCodes(): array
    {
        $languageCodes = [I18n::getDefaultLocale()];

        foreach ((new Folder(APP . DS . 'Locale'))->read()[0] as $languageCode) {
            $languageCodes[] = $languageCode;
        }

        return $languageCodes;
    }

    /**
     * @param string $languageCode
     * @return string
     */
    public static function getLanguageCodePrefix(string $languageCode): string
    {
        return explode('_', $languageCode)[0];
    }

    /**
     * @return array
     */
    public static function getLanguageCodePrefixes(): array
    {
        $result = [];

        foreach (self::getLanguageCodes() as $languageCode) {
            $result[] = explode('_', $languageCode)[0];
        }

        return $result;
    }

    /**
     * @param array|null $replace
     * @return array
     */
    public static function getLanguageLabels(array $replace = null): array
    {
        $languageLabels = [];
        $languageLabelMap = self::getLanguageLabelMap();

        foreach (self::getLanguageCodes() as $languageCode) {
            if ($replace !== null && array_key_exists($languageCode, $replace)) {
                $languageLabels[] = $replace[$languageCode];
                continue;
            }

            $languageLabels[] = $languageLabelMap[$languageCode];
        }

        return $languageLabels;
    }

    /**
     * @param string $languageCodePrefix
     * @return mixed|null
     */
    public static function getFullLanguageCode(string $languageCodePrefix)
    {
        foreach (self::getLanguageCodes() as $languageCode) {
            if (explode('_', $languageCode)[0] === $languageCodePrefix) {
                return $languageCode;
            }
        }

        return null;
    }

    /**
     * @param string $pageTitle
     * @param string $domain
     */
    public static function loadPageLocale(string $pageTitle, string $domain = 'page')
    {
        if (I18n::getLocale() !== I18n::getDefaultLocale()) {
            I18n::setTranslator(
                $domain,
                new MessagesFileLoader($pageTitle, I18n::getLocale(), 'po'),
                I18n::getLocale()
            );
        }
    }
}
