<?php

declare(strict_types=1);

namespace Infotechnohelp\Localization\Controller\Api;

use App\Controller\Api\AppController;
use Cake\Filesystem\File;
use Cake\I18n\Parser\PoFileParser;
use Cake\Routing\Router;

/**
 * Class PhrasesController
 * @package Infotechnohelp\Localization\Controller\Api
 */
class PhrasesController extends AppController
{

    public function initialize()
    {
        parent::initialize();

        $this->Auth->allow();
    }

    public function index()
    {
        $phrases = json_decode(
            file_get_contents('js/Locale/' . $this->Cookie->read('language') . '/default.json'),
            true
        );

        $result = [];

        foreach ($this->_getRequestData()['keys'] as $key) {
            $result[$key] = $phrases[$key];
        }

        $this->_setResponse($result);
    }
}
