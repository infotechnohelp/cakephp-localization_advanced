<?php

declare(strict_types=1);

namespace Infotechnohelp\Localization\Controller\Component;

use Infotechnohelp\JsonApi\Traits\RouteParser;
use Cake\Controller\Component;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\Http\Exception\InternalErrorException;
use Cake\I18n\I18n;
use Infotechnohelp\Localization\Lib\LocaleManager;

/**
 * Class LanguagesComponent
 * @package Infotechnohelp\Localization\Controller\Component
 */
class LocalizationComponent extends Component
{
    use RouteParser;

    /**
     * @param $Request
     * @return bool
     */
    private function routeContainsIgnoredPathPrefix($Request)
    {
        foreach (Configure::read('Infotechnohelp.Localization')['ignoredPathPrefixes'] as $prefix) {
            if ($this->routeContains($Request, $prefix)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param Event $event
     */
    public function beforeFilter(Event $event)
    {

        if (count(LocaleManager::getLanguageCodes()) <= 1) {
            return;
        }

        $controller = $this->_registry->getController();

        $controller->Cookie->configKey('language', [
            'encryption' => false,
            'expires' => '1 month',
            //'domain' => '/',
        ]);

        $Request = $controller->getRequest();

        if ($this->routeContainsIgnoredPathPrefix($Request)) {
            return;
        }

        $query = $Request->getQuery();

        $languageParameter = $Request->getParam('language');

        if ($languageParameter !== false) {
            $controller->Cookie->write('language', LocaleManager::getFullLanguageCode($languageParameter));
        }

        if (!empty($query) && array_key_exists('lang', $query)) {
            $lang = $query['lang'];
            if (!empty($lang)) {
                $controller->Cookie->write('language', $lang);
            }
        }

        $languageCookie = $controller->Cookie->read('language');

        if (!empty($languageCookie)) {
            I18n::setLocale($languageCookie);
        }

        $queryString = $Request->getUri()->getQuery();

        if (!$languageParameter) {
            $url = (!empty($languageCookie)) ?
                DS . LocaleManager::getLanguageCodePrefix($languageCookie) :
                DS . LocaleManager::getLanguageCodePrefix(I18n::getDefaultLocale());

            $url .= $Request->getPath();

            if (!empty($queryString)) {
                $url .= '?' . $queryString;
            }

            $controller->redirect($url);

            return;
        }

        if (LocaleManager::getFullLanguageCode($languageParameter) !== $languageCookie) {
            $exploded = explode('/', $Request->getPath());

            if ($exploded[1] !== $languageParameter) {
                throw new InternalErrorException('Incorrect path structure encountered');
            }

            $exploded[1] = LocaleManager::getLanguageCodePrefix($languageCookie);


            $url = implode('/', $exploded);

            if (!empty($queryString)) {
                $url .= '?' . $queryString;
            }

            $controller->redirect($url);
        }
    }
}
