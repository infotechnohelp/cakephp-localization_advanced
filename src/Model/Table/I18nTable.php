<?php
namespace Infotechnohelp\Localization\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Class I18nTable
 * @package Infotechnohelp\Localization\Model\Table
 */
class I18nTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('i18n');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('locale')
            ->maxLength('locale', 6)
            ->requirePresence('locale', 'create')
            ->allowEmptyString('locale', false);

        $validator
            ->scalar('model')
            ->maxLength('model', 255)
            ->requirePresence('model', 'create')
            ->allowEmptyString('model', false);

        $validator
            ->integer('foreign_key')
            ->requirePresence('foreign_key', 'create')
            ->allowEmptyString('foreign_key', false);

        $validator
            ->scalar('field')
            ->maxLength('field', 255)
            ->requirePresence('field', 'create')
            ->allowEmptyString('field', false);

        $validator
            ->scalar('content')
            ->requirePresence('content', 'create')
            ->allowEmptyString('content', false);

        return $validator;
    }
}
