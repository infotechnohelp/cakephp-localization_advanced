<?php
declare(strict_types=1);

namespace Infotechnohelp\Localization\Model\Entity;

use Cake\ORM\Entity;

/**
 * I18n Entity
 *
 * @property int $id
 * @property string $locale
 * @property string $model
 * @property int $foreign_key
 * @property string $field
 * @property string $content
 */
class I18n extends Entity
{
    //@codingStandardsIgnoreStart
    protected $_accessible = [
        'locale' => true,
        'model' => true,
        'foreign_key' => true,
        'field' => true,
        'content' => true
    ];
    //@codingStandardsIgnoreEnd
}
