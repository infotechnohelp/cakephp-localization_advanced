# cakephp-localization_advanced


NB!!! Depends on `infotechnohelp/cakephp-json-api_basic`!

## Implementation

### `composer require infotechnohelp/cakephp-localization_advanced`


### 1. Add plugin

#### Core implementation

##### vendor/infotechnohelp/cakephp-core_.../config/bootstrap.php

```
/** @var \App\Application $Application */
$Application = new \App\Application(CONFIG);
$Application->addPlugin('Infotechnohelp/Localization', ['routes' => true]);
```

#### App implementation

##### src/Application.php → bootstrap()

`$this->addPlugin('Infotechnohelp/Localization', ['routes' => false]);

or

`bin/cake plugin load Infotechnohelp/Localization`


### 2. Add component

#### Core implementation

##### vendor/infotechnohelp/cakephp-core_.../src/Controller/AppController.php → initialize()

`$this->loadComponent('Infotechnohelp/Localization.Localization');`

#### App implementation

##### src/Controller/AppController.php → initialize()

`$this->loadComponent('Infotechnohelp/Localization.Localization');`

### 3. Set localization config

`config/bootstrap.php`

Configure::write('Infotechnohelp.Localization', [
    'ignoredPathPrefixes' => ['auth-api', 'api', 'user-activities',],
]);

### 4. Migrate migrations

`bin/cake migrations migrate --plugin Infotechnohelp/Localization`

Tables: i18nTable

## Routes

Automatically indicates, which locales are included in APP/src/Locale

en_US, et_EE, ru_RU

.../en/... → Locale is set to en_US
.../et/... → Locale is set to et_EE
.../ru/... → Locale is set to ru_RU

Does not apply for api paths (.../api/...)

##

.../...?lang=en_US → Locale is set to en_US, redirected to .../en/...?lang=en_US

if language cookie exists and there is neither a route locale or GET query parameter specified, a language cookie value will be used

If language cookie value is en_US and a request is .../..., the request will be redirected to .../en/...

NB!!! GET query is more important than route locale.

Importance levels (Priority):
1. GET query parameter
2. Route locale
3. Language cookie
4. Default locale


## LanguagesComponent



## API
.../localization/api/phrases

NB!!! Currently works with default.po only

Request payload structure:

[
	'keys' => ['key1', 'key2'],
]


### Locale Manager

### i18n localization, ModelManager (saving localized entities)

```
use Cake\ORM\Behavior\Translate\TranslateTrait;

class Article extends Entity
{
    use TranslateTrait; 
}
```

### Phrases loader

Loads JS files

### LocaleShell

`bin/cake locale js`

Generates:

webroot/js/Locale/en_US/default.js
webroot/js/Locale/en_US/default.json

### vendor/infotechnohelp/cakephp-localization_advanced/webroot/js/locale.js


