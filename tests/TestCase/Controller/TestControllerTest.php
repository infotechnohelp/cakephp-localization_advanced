<?php

namespace Infotechnohelp\Authentication\Test\TestCase\Controller;

use Cake\Core\Configure;
use Cake\TestSuite\IntegrationTestCase;

class TestControllerTest extends IntegrationTestCase
{
    /**
     * @throws \PHPUnit\Exception
     */
    public function testAllowedActionRedirect()
    {
        $this->get('tests/allowed-action');
        // @todo Hack required because on localhost '/\' at the beginning, '/' in Gitlab CI
        $this->assertContains('en/tests/allowed-action', $this->_response->getHeader('Location')[0]);
    }
}
