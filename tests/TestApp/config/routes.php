<?php

declare(strict_types=1);

use Infotechnohelp\Localization\Lib\LocaleManager;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;
use \Cake\Routing\RouteBuilder;

Router::defaultRouteClass(DashedRoute::class);

Router::scope('/', function (RouteBuilder $routes) {
    $routes->fallbacks(DashedRoute::class);
});

foreach (LocaleManager::getLanguageCodePrefixes() as $language) {
    Router::scope('/' . $language, ['language' => $language], function (RouteBuilder $routes) {
        $routes->fallbacks(DashedRoute::class);
    });
}
