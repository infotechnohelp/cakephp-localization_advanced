<?php

namespace App\Controller;

use Cake\Controller\Controller;

class AppController extends Controller
{
    /**
     * Initialize
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('Cookie');

        $this->loadComponent('Infotechnohelp/Localization.Localization');
    }
}