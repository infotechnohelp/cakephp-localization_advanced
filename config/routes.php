<?php

use Cake\Routing\Router;
use Infotechnohelp\Localization\Lib\LocaleManager;
use Cake\Core\Configure;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Route\DashedRoute;

$routerCallbacks = Configure::read('routerCallbacks');

$requiredRouterCallbacks = [
    $routerCallbacks['project'],
    $routerCallbacks['cakephp-core'],
];

foreach ($requiredRouterCallbacks as $routerCallback) {
    foreach (LocaleManager::getLanguageCodePrefixes() as $language) {
        Router::scope('/' . $language, ['language' => $language], $routerCallback);
    }
}

Router::plugin(
    'Infotechnohelp/Localization',
    ['path' => '/localization'],
    function (RouteBuilder $routes) {
        $routes->prefix('api', function (RouteBuilder $routes) {
            $routes->fallbacks(DashedRoute::class);
        });
        $routes->fallbacks(DashedRoute::class);
    }
);